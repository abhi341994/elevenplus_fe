import { Injectable} from '@angular/core';
import { Http,Response , Headers ,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from './user';
import {AuthService} from '../authentication.service';
import 'rxjs/add/operator/map' ;
import 'rxjs/add/operator/do';	
import 'rxjs/add/operator/catch';

@Injectable()
export class DataService
{
    private url= 'http://localhost:8080';
    constructor(private http:Http, private authService:AuthService)
    {
    }

getMethod():Observable<User[]>
{
    return this.http.get(this.url.concat('/list'))
    .map((res:Response) => <User[]>res.json() )
    .do(data=>console.log(JSON.stringify(data)))
    .catch(this.handleError);
}

getTest():Observable<boolean>{
    let header = new Headers({'Content-Type' : 'application/json',
                                'Authorization' : 'Bearer' + this.authService.getToken()});
    let optn = new RequestOptions({headers:header});
    return this.http.get('http://localhost:8080/elevenPlus/test',optn)
    .map((res:Response)=> { console.log(res)})
    .catch(this.handleError);

}

createEntry(t:User):Observable<boolean>{
    let header = new Headers({'Content-Type' : 'application/json'});
    let optn = new RequestOptions({headers:header});
    return this.http.post('http://localhost:8080/elevenPlus/sign-up' , JSON.stringify(t),optn)
    .map((response:Response) => {
        
        //let token=response.json() && response.json().bearer;
        let token = response.text();
        console.log(response.headers);
        console.log(response.text());
        if(token){
          //  localStorage.setItem('currentUser',JSON.stringify({username:t.email, token: token}));
            console.log('token generated');
            return true;
        }
        else{
            console.log('flaose token ');
            return false;

        }
    })
    .catch(this.handleError)


}

checkOTP(t:number):Observable<boolean>{
    let header = new Headers({"Content-Type" : "application/json"});
    let optn = new RequestOptions({headers:header});
    return this.http.post("http://localhost:8080/elevenPlus/verify" , t,optn)
    .map((response:Response) => {
        //let token=response.json() && response.json().bearer;
        console.log(response.headers);
        console.log(response.text());
    })
    .catch(this.handleError)

}

private extractData(res:Response){
    let body = res.json();
    return body.data || {}

}

private handleError(error: Response){
    console.error(error);
    return Observable.throw(error.json().error());
}

}