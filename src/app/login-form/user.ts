export class User{
    id:string;
    role: String;
    firstName:string;
    lastName:string;
    email:any;
    mobile:string;
    password:any;
    confirmPassword:any;
    sfirstName:string;
    slastName:string;
    gender:string;
    sgender:string;
    
        constructor(
           id:string,
          role: String,
           firstName:string,
           lastName:string,
          email:any,
           mobile:string,
            password:any,
           confirmPassword:any,
           sfirstName:string,
           sLastName:string,
           gender:string,
           sgender:string
        )
        
        {
            this.id=id;
            this.role=role;
    this.firstName=firstName;
    this.lastName=lastName;
    this.email=email;
    this.mobile=mobile;
    this.password=password;
    this.confirmPassword=confirmPassword;
    this.sfirstName=sfirstName;
    this.slastName=sLastName;
    this.gender=gender;
    this.sgender=sgender;
        }
    }