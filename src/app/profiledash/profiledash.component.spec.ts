import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfiledashComponent } from './profiledash.component';

describe('ProfiledashComponent', () => {
  let component: ProfiledashComponent;
  let fixture: ComponentFixture<ProfiledashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfiledashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfiledashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
