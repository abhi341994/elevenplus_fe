import { Injectable} from '@angular/core';
import { Http,Response , Headers ,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ChildDetail } from './profile';
import {AuthService} from '../authentication.service';
import 'rxjs/add/operator/map' ;
import 'rxjs/add/operator/do' ;
import 'rxjs/add/operator/catch';

@Injectable()
export class ProfileService
{
    private url= "http://localhost:8080/elevenPlus/userlist";
    constructor(private http:Http, private authService:AuthService)
    {
    }

getMethod():Observable<ChildDetail[]>
{
    let header = new Headers({'Content-Type' : 'application/json',
    'Authorization' : 'Bearer' + this.authService.getToken()});
    let optn = new RequestOptions({headers:header});
    return this.http.get(this.url,optn)
    .map((res:Response) => <ChildDetail[]>res.json() )
    .do(data=>console.log(JSON.stringify(data)))
    .catch(this.handleError);
}

createEntry(t:ChildDetail):Observable<ChildDetail>{
    let header = new Headers({'Content-Type' : 'application/json',
                            'Authorization' : 'Bearer' + this.authService.getToken()});
    let optn = new RequestOptions({headers:header});
    return this.http.post("http://localhost:8080/elevenPlus/res" , JSON.stringify(t),optn)
    .map(this.extractData)
    .catch(this.handleError)


}
DelById(t:number):Observable<ChildDetail>
{
    let header = new Headers({'Content-Type' : 'application/json',
    'Authorization' : 'Bearer' + this.authService.getToken()});
    let optn = new RequestOptions({headers:header});
  return this.http.delete("http://localhost:8080/elevenPlus/Qa/"+t.toString(),optn)
         .map(() => null)
         .catch(this.handleError);
}


private extractData(res:Response){
    let body = res.json();
    return body.data || {}

}

private handleError(error:Response){
    console.error(error);
    return Observable.throw(error.json().error());
}

}
