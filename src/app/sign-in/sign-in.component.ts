import { Component, OnInit } from '@angular/core';
import {AuthService} from '../authentication.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  
  constructor(private router: Router, private authService : AuthService ) { }

  ngOnInit() {

  }


  Sub(email,password){
    console.log(email);
    console.log(password);
    this.authService.login(email,password)
                        .subscribe( result =>{
                          if(result === true)
                            {
                              console.log("valid");
                              this.router.navigate(['/profile']);
                            }
                            else{
                              // login failed
                            console.log('username is wrong ');
                           
                            }
                        })   


  }
}
