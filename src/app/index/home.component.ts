import { Component, OnInit,Renderer2 } from '@angular/core';
import {MyService} from '../script.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[MyService]
})
export class HomeComponent implements OnInit {

  constructor(private sc:MyService,private _renderer2: Renderer2) { }

  ngOnInit() {
    this.sc.setscript(this._renderer2);
  }

}
