import { HomeComponent } from './index/home.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { FaqComponent } from './faq/faq.component';
import {CreateExamComponent} from './create-exam/create-exam.component';
import {ExamPackListingComponent} from './exam-pack-listing/exam-pack-listing.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { SignInComponent} from './sign-in/sign-in.component';
import {LoginFormsComponent} from './login-form/login-forms.component';
import {AboutusComponent} from './aboutus/aboutus.component';
import {ProfiledashComponent} from './profiledash/profiledash.component';
import {CardsComponent} from './cards/cards.component';
import {SelectChildComponent} from './select-child/select-child.component';


const routes: Routes = [
   {path: 'faq', component: FaqComponent},
   {path: 'listing', component: ExamPackListingComponent},
   {path: 'createExam', component: CreateExamComponent},
   {path: 'index', component: HomeComponent},
   {path: 'contact', component: ContactusComponent },
   {path: 'register', component: LoginFormsComponent},
   {path: 'login', component:SignInComponent},
   {path: 'AboutUs', component:AboutusComponent},
   {path: 'profile', component:ProfiledashComponent},
   {path:'enrol', component:SelectChildComponent},
   {path: '', redirectTo: 'index', pathMatch: 'full'},
   {path: '**', component: PagenotfoundComponent }
];

@NgModule ({
  imports: [
    RouterModule.forRoot(routes, {enableTracing: true})
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
