import { Component, OnInit,Renderer2 } from '@angular/core';
import {MyService} from '../script.service'

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css'],
  providers: [MyService]
})
export class FaqComponent implements OnInit {

  constructor(private sc:MyService,private _renderer2: Renderer2) { }

    ngOnInit() {
      this.sc.setscript(this._renderer2);
    }

}
