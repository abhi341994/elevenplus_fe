import { Component, OnInit ,Input } from '@angular/core';
import { IMultiSelectOption , IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import {ExamPackModel} from '../cards/exam-pack-model';
import {SharedService} from '../SharedSvc';

@Component({
  selector: 'app-select-child',
  templateUrl: './select-child.component.html',
  styleUrls: ['./select-child.component.css']
})
export class SelectChildComponent implements OnInit {

  optionsModel: number[] = [];
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 3,
    displayAllSelectedText: true
};
myTexts: IMultiSelectTexts = {
  checkAll: 'Select all',
  uncheckAll: 'Unselect all',
  checked: 'item selected',
  checkedPlural: 'items selected',
  searchPlaceholder: 'Find',
  searchEmptyResult: 'Nothing found...',
  searchNoRenderText: 'Type in search box to see results...',
  defaultTitle: 'Select',
  allSelected: 'All selected',
};

examPack: ExamPackModel;


myOptions: IMultiSelectOption[] = [];

  constructor(private svc:SharedService) { }

  ngOnInit() {
    this.myOptions = [
      { id: 1, name: 'Student Name', isLabel: true },
      { id: 2, name: 'Mukesh', parentId: 1 },
      { id: 3, name: 'Rohan', parentId: 1 },
      { id: 4, name: 'Sudha', parentId: 1 },
      // { id: 5, name: 'Colors', isLabel: true },
      { id: 6, name: 'Barkha', parentId: 5 },
      { id: 7, name: 'Abhishek', parentId: 5 },
      { id: 8, name: 'Pankaj', parentId: 5 }
  ];

  this.examPack = SharedService.examPck;
}
  onChange() {
    console.log(this.optionsModel);
  }

}

