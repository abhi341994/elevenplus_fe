import { Component, OnInit } from '@angular/core';
import {MyService} from './service';
import {Model} from './ExamPack';

@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.css'],
  providers: [MyService]
})
export class CreateExamComponent implements OnInit {

  examdata: Model[];

    ngOnInit() {
      this.s.get().subscribe(t => this.examdata = t);
    }
    constructor(private s: MyService)  {

    }
    // saving data
    addDetail() {
      this.s.saveData(this.m).subscribe(yt => console.log(yt));
    }


    m:Model;
    obj:Model[] = [];

   myData(test) {
     let sub: any[] = [];
     let id: any = "" ;
     let ntest=test.value;
     let check = document.getElementsByName('chk[]');
     for ( var i = 0 ; i < check.length; i++) {
        if ((check[i]  as HTMLInputElement).checked) {
            sub.push((check[i] as HTMLInputElement).value);
          }
      }
     this.obj.push(new Model(this.class , test.value , sub,id));
     this.m=new Model(this.class , ntest , "math",id);
     console.log(this.m);
     this.addDetail();
     alert("Data Added"+test.value);
     localStorage.setItem("Exam Data",JSON.stringify(this.obj));
   }
   i:any=0;
   class;
   mydata2()
   {

   }

    getClass(s)

    {
      this.class = s;
    }

    //get Data

    mypack;
    shw = false;
    fetchData()
    {
      this.shw = true;
      var data = localStorage.getItem('Exam Data');
      this.mypack = JSON.parse(data);
    }
}
