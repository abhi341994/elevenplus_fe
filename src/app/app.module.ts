import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FaqComponent } from './faq/faq.component';
import {ExamPackListingComponent} from './exam-pack-listing/exam-pack-listing.component';
import { AppRoutingModule } from './app-routing.module';
import { FooterComponent } from './footer/footer.component';
import { HttpModule } from '@angular/http';
import {MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { CreateExamComponent } from './create-exam/create-exam.component';
import { HomeComponent } from './index/home.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { SignInComponent} from './sign-in/sign-in.component';
import {LoginFormsComponent} from './login-form/login-forms.component';
import { FormsModule } from '@angular/forms';
import { EqualValidator } from './login-form/password.match.directive';
import { AboutusComponent } from './aboutus/aboutus.component';
import {AuthService} from './authentication.service';
import {ProfiledashComponent} from './profiledash/profiledash.component';
import {CardsComponent} from './cards/cards.component';
import {SelectChildComponent} from './select-child/select-child.component';
import {SharedService} from './SharedSvc'

@NgModule({
  declarations: [
    AppComponent,
    FaqComponent,
    NavbarComponent,
    FooterComponent,
    ExamPackListingComponent,
    CreateExamComponent,
    HomeComponent,
    ContactusComponent,
    PagenotfoundComponent,
    SignInComponent,
    LoginFormsComponent,
    EqualValidator,
    AboutusComponent,
    ProfiledashComponent,
    CardsComponent,
    SelectChildComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule , HttpModule, FormsModule, MultiselectDropdownModule
  ],
  providers: [AuthService, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
