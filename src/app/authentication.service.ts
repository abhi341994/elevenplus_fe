import {Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Injectable()
export class AuthService
{
    private url= 'http://localhost:8080';
    constructor(private http:Http)
    {
    }
    login(user:string,password:string): Observable<boolean>
    {
        let header = new Headers({'Content-Type' : 'application/json'});
    let optn = new RequestOptions({headers:header});
    return this.http.post('http://localhost:8080/elevenPlus/sign-in' , JSON.stringify({username:user, password: password}),optn)
    .map((response:Response) => {
        let token=response.text();
        console.log(token);
        if(token){
            localStorage.setItem('currentUser',JSON.stringify({username:user, token: token}));
            return true;
        }
        else{
            return false;
        }
    })
    .catch(this.handleError)
    }

    getToken():String{
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser && currentUser.token;
        return token ? token : '';
    }

    logout():void{
        localStorage.removeItem('currentUser');
    }



    private extractData(res:Response){
    let body = res.json();
    return body.data || {}

}

private handleError(error:Response){
    console.error(error);
    return Observable.throw(error.json().error());
}
}
