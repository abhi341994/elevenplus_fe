import { Injectable } from "@angular/core";
import { Response,Http,RequestOptions,Headers} from "@angular/http";
import { Observable} from 'rxjs/Observable';
import {ExamPackModel} from './exam-pack-model';
import {SelectedExamPack} from './selected-exam-pack';
import {AuthService} from '../authentication.service';
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";



@Injectable()

export class CardService
{
    selectedExam:SelectedExamPack;
    url="assets/json/cards.json";
    url2= 'http://localhost:8080/examPack/getExamPack';
    constructor(private http:Http, private authService:AuthService)
    {}

    getMethod():Observable<ExamPackModel[]>
    {
        let header = new Headers({'Content-Type' : 'application/json',
        'Authorization' : 'Bearer' + this.authService.getToken()});
        let optn = new RequestOptions({headers:header});
        return this.http.get(this.url2,optn).map((res:Response)=><ExamPackModel[]>res.json());
    }


examPackSelected(selectedExamPack:SelectedExamPack):Observable<boolean>
 {
    let header = new Headers({"Content-Type" : "application/json"});
    let  optn = new RequestOptions({headers:header});
     console.log(selectedExamPack.examPackId);

    return this.http.post("http://localhost:8080/web",JSON.stringify(selectedExamPack),optn)
            .map((resp: Response) => {
                console.log(resp.json());
                return true;
            })
            .catch(this.handleError);
    }

     private handleError(error:Response){
        console.error(error);
        return Observable.throw(error.json().error());
    }
}