import { Component, OnInit } from '@angular/core';
import {ExamPackModel} from './exam-pack-model';
import { CardService } from './cards.service';
import {SelectedExamPack} from './selected-exam-pack';
import {Router} from '@angular/Router';
import {SharedService} from '../SharedSvc';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css'],
  providers:[CardService]
})
export class CardsComponent implements OnInit{

  examPack: ExamPackModel[];
  data;
  examIdSelected: SelectedExamPack= new SelectedExamPack();
  constructor(private serviceFile: CardService , private svc:SharedService) { 
      
    this.serviceFile.getMethod().subscribe(data => this.examPack = data);
      // alert(this.examPack)

  }
  ngOnInit() {
    
  }

  // call get exam pack
selectStudent()
{
 // this.route.
}

  sendPack(s){
    console.log(s);
    SharedService.examPck=s;
  }
  show(examId)
  {  
    console.log(examId);
    this.examIdSelected.examPackId = examId;
    console.log('============');
    console.log(this.examIdSelected);
    this.serviceFile.examPackSelected(this.examIdSelected).subscribe(result => {
      console.log(result);
    }
  )

  }
}
